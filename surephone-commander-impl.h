/**
 * @file surephone-commander-impl.h
 */

#include <string>
#include <cstring>
#include <iostream>

#include "surephone-commander.h"
#include "surephone-commander-config.h"

int lsUser(std::string &r, const SurephoneCommanderConfig &config);
int lsPids(std::string &r, const SurephoneCommanderConfig &config);
int pushMessage(std::string &r, const SurephoneCommanderConfig &config);
int manualRegisterPid(std::string &r, SurephoneCommanderConfig &config);
int registerPid(std::string &r, SurephoneCommanderConfig &config);
int unregisterPid(std::string &r, SurephoneCommanderConfig &config);
