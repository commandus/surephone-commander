#include "surephone-commander-impl.h"

#include <cctype>
#include <sstream>
#include <iomanip>
#include <curl/curl.h>

/**
  * @brief CURL write callback
  */
static size_t write_string(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
   return size * nmemb;
}

/**
 * @brief Encode URL
 * @see https://stackoverflow.com/questions/154536/encode-decode-urls-in-c
 */
static std::string urlEncode
(
	const std::string &value
) 
{
	std::ostringstream escaped;
	escaped.fill('0');
	escaped << std::hex;
	for (std::string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) 
	{
		std::string::value_type c = (*i);
		// Keep alphanumeric and other accepted characters intact
		if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') 
		{
			escaped << c;
			continue;
		}

		// Any other characters are percent-encoded
		escaped << std::uppercase;
		escaped << '%' << std::setw(2) << int((unsigned char) c);
		escaped << std::nouppercase;
	}
	return escaped.str();
}

/**
  * @brief GET
  */
static int curl_get
(
	std::string &retval,
	const std::string &url
)
{
	CURL *curl = curl_easy_init();
	if (!curl)
		return CURLE_FAILED_INIT; 
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &write_string);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &retval);
    CURLcode res = curl_easy_perform(curl);
	long http_code;
    if (res != CURLE_OK)
	{
		retval = curl_easy_strerror(res);
		http_code = res;
	}
	else
	{
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);
	}
	curl_easy_cleanup(curl);
	return http_code;
}

int lsUser(std::string &r, const SurephoneCommanderConfig &config)
{
	std::stringstream ss;
	ss << config.url << "/lsusr?userid=" << config.registration.user 
		<< "&created=" << config.registration.created
		<< "&accesskey=" << config.registration.accesskey;
	int c = curl_get(r, ss.str());
	return c;
}

int pushMessage(std::string &r, const SurephoneCommanderConfig &config)
{
	std::stringstream ss;
	ss << config.url << "/spk?userid=" << config.registration.user 
		<< "&created=" << config.registration.created
		<< "&accesskey=" << config.registration.accesskey
		<< "&title=" << urlEncode(config.subject)
		<< "&text=" << urlEncode(config.body)
		<< "&icon=" << urlEncode(config.icon)
		<< "&click_action=" << urlEncode(config.link);
	std::string s = ss.str();

	int c = 200;
	for (std::vector<std::string>::const_iterator it = config.keys.begin(); it != config.keys.end(); ++it)
	{
		c = curl_get(r, s + "&key=" + urlEncode(*it));
		if (config.verbosity > 2) 
		{
			std::cerr << "Send: " << s + "&key=" + urlEncode(*it) << std::endl;
		}
		r.append("\n");
		if ((c < 200) || (c > 299))
			break;
	}
	return c;
}

int manualRegisterPid(std::string &r, SurephoneCommanderConfig &config)
{
	config.registrations[config.registration.user] = config.registration;
	config.save();
	return 0;
}

int lsPids(std::string &r, const SurephoneCommanderConfig &config)
{

	std::stringstream ss;
	for (std::map<size_t, Registration>::const_iterator it(config.registrations.begin()); it != config.registrations.end(); ++it)
	{
		ss << it->second.user << " "
			<< it->second.created << " " 
			<< it->second.accesskey << " " 
			<< std::endl;
	}
	r = ss.str();
}

int registerPid(std::string &r, SurephoneCommanderConfig &config)
{
	std::stringstream ss;
	ss << config.url << "/addusrdev?parent_id=" << config.pid 
		<< "&instance=" << urlEncode(config.instance)
		<< "&name=" << urlEncode(config.name);
	std::string s = ss.str();
	int c = curl_get(r, s);
	if ((c >= 200) && (c < 400) && (r != "false")) {
		config.registration.set(r);
		config.pid = config.registration.user;
		config.registrations[config.pid ] = config.registration;
		config.save();
	}
	return c;
}

int unregisterPid(std::string &r, SurephoneCommanderConfig &config)
{
	std::stringstream ss;
	ss << config.url << "/rmusr?userid=" << config.registration.user 
		<< "&created=" << config.registration.created
		<< "&accesskey=" << config.registration.accesskey;
	int c = curl_get(r, ss.str());
	if (c >= 200 && c < 300 && r == "true") {
		std::map<size_t, Registration>::iterator it = config.registrations.find(config.registration.user);
		if (it != config.registrations.end()) {
			config.registrations.erase(it);
			config.save();
		}
	}
	return c;
}
