/**	
  * SurePhone Commander options
  * @file surephone-commander-config.h
  **/

#ifndef SUREPHONE_COMMANDER_CONFIG_H
#define SUREPHONE_COMMANDER_CONFIG_H

#include <string>
#include <vector>
#include <map>
#include <iostream>

#define CMD_NONE			0
#define CMD_LS_USER			1
#define CMD_SET				2
#define CMD_PUSH			3
#define CMD_LS_PID			4
#define CMD_REGISTER		5
#define CMD_REREGISTER		6
#define CMD_UNREGISTER		7

#define MSG_INTERRUPTED		"Interrupted"

class Registration
{
public:
	size_t user;
	int created;
	int accesskey;
	bool read(std::istream &strm);
	void write(std::ostream &strm);
	Registration();
	Registration(const Registration &c);
	Registration& operator=(const Registration& other);
	bool set(const std::string &json);
};

/**
 * Command line interface (CLI) tool configuration structure
 */
class SurephoneCommanderConfig
{
private:
	/**
	* Parse command line into SurephoneCommanderConfig class
	* Return 0- success
	*        1- show help and exit, or command syntax error
	*        2- output file does not exists or can not open to write
	**/
	int parseCmd
	(
		int argc,
		char* argv[]
	);
	int errorcode;
	
	size_t read(std::istream &strm);
	void write(std::ostream &strm);

public:
	int cmd;										///< 0- list of recipients, 1- set default, 2- push, 3- list of pids, 4- register, 5- unregister
	std::string url;								///< service url
	std::string file_name;							///< config file
	std::vector<std::string> keys;					///< recipient account numbers or phone numbers
	std::string subject;							///< subject
	std::string body;								///< message body
	std::string icon;
	std::string link;
	size_t pid;										///< for registration only
	std::string instance;							///< for registration only
	std::string name;								///< for registration only
	std::map<size_t, Registration> registrations;
	Registration registration;
	int list_offset;
	int list_size;
	int verbosity;
	SurephoneCommanderConfig();
	SurephoneCommanderConfig
	(
		int argc,
		char* argv[]
	);
	size_t load();
	void save();
	int error();
};

#endif
