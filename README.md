# SurePhone commander

Send push notification to mobile users, web push notification to web browsers (including mobile).

Recipients has identified by account#, phone#. 

## How to add regsitration button to your site

https://github.com/commandus/surephone-html5

## Usage

### Register to send notifications

First of all you need create account:

```
./surephone-commander -r
```

or supply your name(optional)

```
./surephone-commander -r --name "My name"
```

Please note each time you make registration, you create a new account.

To see all registered accounts use -l option (refer to section "List of registrations").

### Register to send and receive notifications

Option --instance allow receive notifications to specified by instance device.

In this case you can also set option --pid - from which account you want to receive notifications.
- instance 	Existing FireBase instance

### Use existing account

Option --reregister does not create a new account instead allow use same account in different user environments.


```
./surephone-commander --reregister -i account# -c number -a number --instance "string"
```

### List of registrations

```
./surephone-commander -l
```

List starts from 0 to 100 by default.

In case you have a lot of registrations, use -o option to set offset (startting with 0) and -O to limit records. 

Valid -O option values are between 1 and 100.

### Remove unused registration

Before you start check accounts using -l option.

```
./surephone-commander -R account#
```

You must supply account# (PID).

### List of recipients

```
./surephone-commander -u -i account#
```

For instance:

```
./surephone-commander -i 149 -u
[[79140000000,"+7-914-000-0000"]]
```

List starts from 0 to 100 by default.

In case you have a lot of registrations, use -o option to set offset (startting with 0) and -O to limit records. 

Valid -O option values are between 1 and 100.

### Send notification by account# (or phone number)

Before you start check accounts using -l option. Your n=must suppply your account number (PID) to send notification.

```
./surephone-commander -m -i account# -s "subject text" -b "body text" -p "icon url" -l "action link"
```

For instance:

```
./surephone-commander -i 149 -m -s Subject -b Body -p "https://commandus.com/favicon.ico" -l "https://commandus.com" 79140000000
```

sends message "Body" to recipient with account# 79140000000 whic subscribes PID 149.

You must supply one or up to 100 account numbers (PIDs).

Options -b, -s values must have UTF-8 encoding.

### Debug log

Use option -vvv

### Alternative configuration

Use option -f <file name>

## Install

At first time build Autoconf

```
autoreconf -fi
automake  --add-missing
```

then

```
./configure
make
sudo make install
```

