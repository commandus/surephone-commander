#include "surephone-commander-config.h"
#include <iostream>
#include <fstream>
#include <argtable2.h>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#include "rapidjson/document.h"

using namespace rapidjson;

static const char* progname = "surephone-commander";
#define DEF_FILE_NAME			".surephone-commander"
#define DEF_SVC_URL				"https://surephone.commandus.com/app"

Registration::Registration()
	: user(0), created(0), accesskey(0)
{
}

Registration::Registration(const Registration &c)
{
	user = c.user;
	created = c.created;
	accesskey = c.accesskey;
}

bool Registration::read(std::istream &strm)
{
	bool r = strm.good();
	strm >> user >> created >> accesskey;
	return r;
}

void Registration::write(std::ostream &strm)
{
	strm << user << " " << created << " " << accesskey;
}

Registration& Registration::operator=(const Registration& other) // copy assignment
{
	if (this != &other) // self-assignment check expected
	{
		user = other.user;
		created = other.created;
		accesskey = other.accesskey;
	}
	return *this;
}

bool Registration::set(const std::string &json)
{
	Document document;
	document.Parse(json.c_str());
	bool r = document.IsArray();
	if (!r)
		return r;
	if (document.Size() < 23)
		return false;
	// [140,151,"",1,1522145596,1522145596,"My name","",0,0,151,1522383576,1522383576,0,1,0,0,0,"","",2889460,0,140411392057888]
	user = document[1].GetInt64();
	created = document[11].GetInt();
	accesskey = document[20].GetInt();
	return r;
}

/**
 * https://stackoverflow.com/questions/2910377/get-home-directory-in-linux-c
 */
static std::string getDefaultConfigFileName()
{
	struct passwd *pw = getpwuid(getuid());
	const char *homedir = pw->pw_dir;
	std::string r(homedir);
	return r + "/" + DEF_FILE_NAME;
}

SurephoneCommanderConfig::SurephoneCommanderConfig()
	: cmd(CMD_NONE), url(DEF_SVC_URL),
	subject(""), body(""), icon(""), link(""),
	list_offset(0), list_size(0),
	pid(0),										///< for registration only
	instance(""),								///< for registration only
	name(""),									///< for registration only
	verbosity(0)
{
	file_name = getDefaultConfigFileName();
}

SurephoneCommanderConfig::SurephoneCommanderConfig
(
	int argc,
	char* argv[]
)
{
	errorcode = parseCmd(argc, argv);
}

size_t SurephoneCommanderConfig::read(std::istream &strm)
{
	size_t r = 0;
	Registration v;
	while (v.read(strm)) {
		registrations[v.user] = v;
		r++;
	}
	return r;
}

void SurephoneCommanderConfig::write(std::ostream &strm)
{
	Registration v;
	for (std::map<size_t, Registration>::const_iterator it = registrations.begin(); it != registrations.end(); ++it) 
	{
		v = it->second;
		v.write(strm);
	}	
}

/**
 * Load pids
 */
size_t SurephoneCommanderConfig::load
(
)
{
	std::ifstream strm(file_name, std::ifstream::in);
	size_t r = read(strm);
	strm.close();
	return r;
}

/**
 * Save pids
 */
void SurephoneCommanderConfig::save
(
)
{
	std::ofstream strm(file_name, std::ofstream::out);
	write(strm);
	strm.close();
}

/**
 * Parse command line into SurephoneCommanderConfig class
 * Return 0- success
 *        1- show help and exit, or command syntax error
 *        2- output file does not exists or can not open to write
 **/
int SurephoneCommanderConfig::parseCmd
(
	int argc,
	char* argv[]
)
{
	struct arg_int *a_pid = arg_int0("i", "pid", "<number>", "Select my registration PID.");
	struct arg_lit *a_cmd_pids = arg_lit0("I", "pids", "List of my saved registration PID.");
	struct arg_lit *a_cmd_users = arg_lit0("u", "users", "List of registered users (receivers). Set --offset, --size to paginate.");
	struct arg_lit *a_cmd_push = arg_lit0("m", "push", "Push message. Set -s -b -i -l.");
	struct arg_lit *a_cmd_reregister = arg_lit0(NULL, "reregister", "Enter already existing registration. Set -i -c -a. Optional --instance");
	struct arg_lit *a_cmd_register = arg_lit0("r", "register", "New registration. Optional --pid --instance --name");
	struct arg_int *a_cmd_unregister = arg_intn("R", "unregister", "<PID>", 0, 100, "Delete my registration.");

	struct arg_str *a_subject = arg_str0("s", "subject", "<Text>", "Message subject");
	struct arg_str *a_body = arg_str0("b", "body", "<Text>", "Message body");
	struct arg_str *a_icon = arg_str0("p", "icon", "<URI>", "http[s]:// icon address.");
	struct arg_str *a_link = arg_str0("l", "link", "<URI>", "https:// action address.");
	struct arg_str *a_keys = arg_strn(NULL, NULL, "<account#>", 0, 100, "Recipient account or phone number.");
	
	struct arg_int *a_mr_created = arg_int0("c", "created", "<number>", "Valid with --reregister.");
	struct arg_int *a_mr_accesskey = arg_int0("a", "accesskey", "<number>", "Valid with --reregister.");
	
	struct arg_int *a_offset = arg_int0("o", "offset", "<number>", "Valid with -l. Default 0.");
	struct arg_int *a_size = arg_int0("O", "size", "<number>", "Valid with -l. Default and max 100.");
	
	struct arg_str *a_file_name = arg_str0("f", "file", "<file>", "Configuration file. Default ~/" DEF_FILE_NAME);
	struct arg_str *a_url = arg_str0(NULL, "svc", "<http[s] URL>", "Service URL. Default " DEF_SVC_URL);

	struct arg_str *a_instance= arg_str0(NULL, "instance", "<string>", "Registration instance (optional)");
	struct arg_str *a_name = arg_str0(NULL, "name", "<string>", "Regisration name (optional)");
	
	struct arg_lit *a_verbosity = arg_litn("v", "verbose", 0, 3, "-vvv - debug.");
	struct arg_lit *a_help = arg_lit0("h", "help", "Show this help");
	struct arg_end *a_end = arg_end(20);

	void* argtable[] = { 
		a_pid, a_cmd_pids, a_cmd_users, a_cmd_push, a_cmd_reregister, a_cmd_register, a_cmd_unregister,
		a_subject, a_body, a_icon, a_link, a_keys, a_mr_created, a_mr_accesskey,
		a_offset, a_size, a_file_name, a_url, 
		a_instance, a_name,
		a_verbosity, a_help, a_end 
	};

	int nerrors;

	// verify the argtable[] entries were allocated successfully
	if (arg_nullcheck(argtable) != 0)
	{
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}
	// Parse the command line as defined by argtable[]
	nerrors = arg_parse(argc, argv, argtable);

	if (a_file_name->count)
		file_name = *a_file_name->sval;
	else
		file_name = getDefaultConfigFileName();
	load();
	
	if (a_pid->count) 
	{
		pid = *a_pid->ival;
		registration = registrations[*a_pid->ival];
		registration.user = pid;
	}

	if (a_cmd_pids->count)
		cmd = CMD_LS_PID;
	else
		if (a_cmd_users->count)
			cmd = CMD_LS_USER;
		else
			if (a_cmd_push->count)
				cmd = CMD_PUSH;
			else
				if (a_cmd_reregister->count)
					cmd = CMD_REREGISTER;
				else
					if (a_cmd_register->count)
						cmd = CMD_REGISTER;
					else
						if (a_cmd_unregister->count) {
							cmd = CMD_UNREGISTER;
							pid = *a_cmd_unregister->ival;
						} else
							cmd = CMD_PUSH;
	if (cmd == CMD_LS_USER)
	{
		if (registration.user == 0)
		{
			std::cerr << "Missed -i <pid>" << std::endl;
			nerrors++;
		}
	}

	if (cmd == CMD_PUSH)
	{
		if (registration.user == 0)
		{
			std::cerr << "Missed -i <pid>" << std::endl;
			nerrors++;
		}
		if (a_subject->count == 0)
		{
			std::cerr << "-s missed." << std::endl;
			nerrors++;
		}
		if (a_body->count == 0)
		{
			std::cerr << "-b missed." << std::endl;
			nerrors++;
		}
		if (a_icon->count == 0)
		{
			std::cerr << "-i missed." << std::endl;
			nerrors++;
		}
		if (a_link->count == 0)
		{
			std::cerr << "-l missed." << std::endl;
			nerrors++;
		}
		if (a_keys->count == 0)
		{
			std::cerr << "No recipient(s)." << std::endl;
			nerrors++;
		}
	}

	if (a_subject->count)
	{
		subject = *a_subject->sval;
	}
	if (a_body->count)
	{
		body = *a_body->sval;
	}
	if (a_icon->count)
	{
		icon = *a_icon->sval;
	}
	if (a_link->count)
	{
		link = *a_link->sval;
	}

	if (a_mr_created->count)
		registration.created = *a_mr_created->ival;
	if (a_mr_accesskey->count)
		registration.accesskey = *a_mr_accesskey->ival;

	if (cmd == CMD_REREGISTER)
	{
		if (registration.user == 0)
		{
			std::cerr << "Missed -i <pid>" << std::endl;
			nerrors++;
		}
		if (registration.created == 0)
		{
			std::cerr << "Missed -c" << std::endl;
			nerrors++;
		}
		if (registration.accesskey == 0)
		{
			std::cerr << "Missed -a" << std::endl;
			nerrors++;
		}
	}
	
	if (a_url->count)
		url = *a_url->sval;
	else
		url = DEF_SVC_URL;

	if (a_instance->count)
		instance = *a_instance->sval;
	else
		instance = "";

	if (a_name->count)
		name = *a_name->sval;
	else
		name = "";

	if (a_offset->count)
		list_offset = *a_offset->ival;

	if (a_size->count)
		list_size = *a_size->ival;

	for (int i = 0; i < a_keys->count; i++)
	{
		keys.push_back(a_keys->sval[i]);
	}

	verbosity = a_verbosity->count;
	
	// special case: '--help' takes precedence over error reporting
	if ((a_help->count) || nerrors)
	{
		if (nerrors)
			arg_print_errors(stderr, a_end, progname);
		std::cerr << "Usage: " << progname << std::endl;
		arg_print_syntax(stderr, argtable, "\n");
		std::cerr << "Push notification to mobile phone command line interface utility." << std::endl;
		arg_print_glossary(stderr, argtable, "  %-25s %s\n");
		arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
		return 1;
	}

	arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
	return 0;
}

int SurephoneCommanderConfig::error()
{
	return errorcode;
}
