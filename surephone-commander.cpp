/**
 * @file surephone-commander.cpp
 */

#include <thread>
#include <string>
#include <cstring>
#include <iostream>
#include <signal.h>
#include <argtable2.h>
#include <curl/curl.h>

#include "surephone-commander-config.h"
#include "surephone-commander-impl.h"

#ifdef _WIN32
void setSignalHandler(int signal)
{
}
#else
void signalHandler(int signal)
{
	switch(signal)
	{
	case SIGINT:
		std::cerr << MSG_INTERRUPTED << std::endl;
		break;
	default:
		break;
	}
}

void setSignalHandler(int signal)
{
	struct sigaction action;
	memset(&action, 0, sizeof(struct sigaction));
	action.sa_handler = &signalHandler;
	sigaction(signal, &action, NULL);
}
#endif

int main(int argc, char** argv)
{
	// Signal handler
	setSignalHandler(SIGINT);

	// In windows, this will init the winsock stuff
	curl_global_init(CURL_GLOBAL_ALL);

	SurephoneCommanderConfig config(argc, argv);
	if (config.error())
		exit(config.error());

	std::string r = "";
	switch (config.cmd) 
	{
		case CMD_LS_USER:
			lsUser(r, config);
			break;
		case CMD_SET:
			break;
		case CMD_PUSH:
			pushMessage(r, config);
			break;
		case CMD_LS_PID:
			lsPids(r, config);
			break;
		case CMD_REREGISTER:
			manualRegisterPid(r, config);
			break;
		case CMD_REGISTER:
			registerPid(r, config);
			break;
		case CMD_UNREGISTER:
			unregisterPid(r, config);
			break;
	}
	std::cout << r << std::endl;
	return 0;
}
